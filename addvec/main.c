#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "addvec.h"

#define LENGTH 1000000
#define RAND_MOD 1000

//
//  main.c
//
//  Note: the testing subject will not receive a copy of this file
//

int serial_addvec( const float* p_a, const float* p_b, float* p_c, int length );

int run_test( const float* p_a, const float*p_b, float* p_c, const float* p_s, int length, 
                          int multiplier, int kind) {
    float epsilon = 0.1f;
    memset(p_c, 0, length*sizeof(float));
    int i;
    for (i = 0; i < multiplier; i++) {
      if (kind == 0)
        addvec_intrinsics( p_a, p_b, p_c, length );
      else
        addvec_gccvectors( p_a, p_b, p_c, length );
    }

    int errcount = 0;
    for( i = 0; i < length; i++ )
    {
        if( (p_c[ i ] < p_s[ i ]-epsilon) || (p_c[ i ] > p_s[ i ] + epsilon) ) {
          if (errcount < 10)
            printf("Error at index %d: solution[i] == %f, computed[i] == %f\n", i, p_s[i], p_c[i]);
          errcount++;
        }
    }
    if (errcount != 0)
      printf(" Errcount %d\n", errcount);
    return errcount;
}
    
int main( void )
{
    srand( time( NULL ) );

    float *p_a;
    if (posix_memalign((void**)&p_a, 16, LENGTH*sizeof(float))) {
      printf("Error allocating aligned memory.");
    }
    float *p_b;
    if (posix_memalign((void**)&p_b, 16, LENGTH*sizeof(float))) {
      printf("Error allocating aligned memory.");
    }
    float *p_c;
    if (posix_memalign((void**)&p_c, 16, LENGTH*sizeof(float))) {
      printf("Error allocating aligned memory.");
    }
    float *p_s;
    if (posix_memalign((void**)&p_s, 16, LENGTH*sizeof(float))) {
      printf("Error allocating aligned memory.");
    }

    //
    //  Note we skip memory initialization for p_c.
    //  A correct function should not require that 
    //  there be assumptions on p_c's initial values.
    //  Notice also that we do not allow the user
    //  to control the alignment of the memory space.
    //  This is part of the issue that we are asking
    //  the user to address in this assignment.
    //

    //
    //  initialize garbage data
    //

    int i = 0;
    for(; i < LENGTH; i++ )
    {
        p_a[ i ] = ( float )( rand() % 1000 ) / 100.0f;
        p_b[ i ] = ( float )( rand() % 1000 ) / 100.0f;
    }

    //
    //  call the user's code
    //

    serial_addvec( p_a, p_b, p_s, LENGTH );

    if (run_test( p_a, p_b, p_c, p_s, LENGTH, 1, 0))
      printf( "Error in intrinsics with aligned inputs.\n");
    if (run_test( p_a+1, p_b+1, p_c+1, p_s+1, LENGTH-4, 1, 0 ))
      printf( "Error in intrinsics with unaligned inputs.\n");

    p_c[LENGTH-1] = 0.0f;
    if (run_test( p_a, p_b, p_c, p_s, LENGTH-1, 1, 0 ) || p_c[LENGTH-1] != 0.0f)
      printf( "Error in intrinsics with nonmultiple input lengths.\n");

    if (run_test( p_a, p_b, p_c, p_s, LENGTH, 1, 1))
      printf( "Error in gcc vectors with aligned inputs.\n");
    if (run_test( p_a+1, p_b+1, p_c+1, p_s+1, LENGTH-4, 1, 1 ))
      printf( "Error in gcc vectors with unaligned inputs.\n");

    p_c[LENGTH-1] = 0.0f;
    if (run_test( p_a, p_b, p_c, p_s, LENGTH-1, 1, 1 ) || p_c[LENGTH-1] != 0.0f)
      printf( "Error in gcc vectors with nonmultiple input lengths.\n");


    run_test( p_a, p_b, p_c, p_s, LENGTH, 1000, 0);
    run_test( p_a, p_b, p_c, p_s, LENGTH, 1000, 1);

    printf( "Run complete\n" );
    free(p_a);
    free(p_b);
    free(p_c);
    free(p_s);
    return 0;
}

int serial_addvec( const float* p_a, const float* p_b, float* p_c, int length )
{
    if( length <= 0 ) return 1;

    int i = 0;
    for(; i < length; i++ )
    {
        p_c[ i ] = p_a[ i ] + p_b[ i ];
    }

    return 0;
}

