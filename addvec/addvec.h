#ifndef __ADDVEC_H__
#define __ADDVEC_H__

int addvec_intrinsics( const float* p_a, const float* p_b, float* p_c, int length );
int addvec_gccvectors( const float* p_a, const float* p_b, float* p_c, int length );

#endif
