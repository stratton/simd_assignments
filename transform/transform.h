#ifndef __TRANSFORM_H__
#define __TRANSFORM_H__

int transform_intrinsics( float p_matrix[ 4 ][ 4 ] , 
	                      float p_vectors[][ 4 ], 
                          float p_outputs[][ 4 ], int length );

int transform_gccvectors( float p_matrix[ 4 ][ 4 ] , 
	                      float p_vectors[][ 4 ], 
                          float p_outputs[][ 4 ], int length );

#endif

