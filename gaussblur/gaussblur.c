#include "gaussblur.h"

//
//  transform.c
//
//  functions: int transform( float p_matrices[][ 4 ][ 4 ], 
//      float p_vectors[][ 4 ], float p_outputs[][ 4 ], int length )
//      arguments:
//          p_matrices: a list of matrices, indexed by row first.
//          p_vectors: a list of vectors to transform using the
//              corresponding matrices.
//          p_outputs: a list of vectors to store the transformed
//              vectors into.
//          int length: The length of each array.
//      function:
//          The function shall transform each vector in p_vectors
//          with the corresponding transformation matrix in
//          p_matrices via dotting each vector on the left by
//          the matrix in question and place each output produced
//          in the corresponding index in p_outputs.
//
//  implement the functions in this file using appropriate vector
//  instructions as directed in the lab instructions.
//

int gaussBlur_intrinsics( float** p_image, float** p_output, int width, int height )
{
    int i = 1;
    for(; i < ( height - 1 ); i++ )
    {
        int j = 1;
        for(; j < ( width - 1 ); j++ )
        {
            p_output[ i ][ j ] = ( 0.5f * p_image[ i ][ j ] ) +
                                 ( 0.125f * p_image[ i - 1 ][ j ] ) +
                                 ( 0.125f * p_image[ i + 1 ][ j ] ) +
                                 ( 0.125f * p_image[ i ][ j - 1 ] ) +
                                 ( 0.125f * p_image[ i ][ j + 1 ] );
        }

        p_output[ i ][ 0 ] = ( 0.625f * p_image[ i ][ 0 ] ) +
                             ( 0.125f * p_image[ i - 1 ][ 0 ] ) +
                             ( 0.125f * p_image[ i + 1 ][ 0 ] ) +
                             ( 0.125f * p_image[ i ][ 1 ] );

        p_output[ i ][ width - 1 ] = ( 0.625f * p_image[ i ][ width - 1 ] ) +
                                     ( 0.125f * p_image[ i - 1 ][ width - 1 ] ) +
                                     ( 0.125f * p_image[ i + 1 ][ width - 1 ] ) +
                                     ( 0.125f * p_image[ i ][ width - 2 ] );
    }

    int j = 1;
    for(; j < ( width - 1 ); j++ )
    {
        p_output[ 0 ][ j ] = ( 0.625f * p_image[ 0 ][ j ] ) +
                             ( 0.125f * p_image[ 1 ][ j ] ) +
                             ( 0.125f * p_image[ 0 ][ j + 1 ] ) +
                             ( 0.125f * p_image[ 0 ][ j - 1 ] );

        p_output[ height - 1 ][ j ] = ( 0.625f * p_image[ width - 1 ][ j ] ) +
                                      ( 0.125f * p_image[ width - 2 ][ j ] ) +
                                      ( 0.125f * p_image[ width - 1 ][ j + 1 ] ) +
                                      ( 0.125f * p_image[ width - 1 ][ j - 1 ] );
    }

    p_output[ 0 ][ 0 ] = ( 0.75f * p_image[ 0 ][ 0 ] ) +
                         ( 0.125f * p_image[ 0 ][ 1 ] ) +
                         ( 0.125f * p_image[ 1 ][ 0 ] );

    p_output[ height - 1 ][ 0 ] = ( 0.75f * p_image[ height - 1 ][ 0 ] ) +
                                  ( 0.125f * p_image[ height - 1 ][ 1 ] ) +
                                  ( 0.125f * p_image[ height - 2 ][ 0 ] );

    p_output[ 0 ][ width - 1 ] = ( 0.75f * p_image[ 0 ][ width - 1 ] ) +
                                 ( 0.125f * p_image[ 0 ][ width - 2 ] ) +
                                 ( 0.125f * p_image[ 1 ][ width - 1 ] );

    p_output[ height - 1 ][ width - 1 ] = ( 0.75f * p_image[ height - 1 ][ width - 1 ] ) +
                                          ( 0.125f * p_image[ height - 1 ][ width - 2 ] ) +
                                          ( 0.125f * p_image[ height - 2 ][ width - 1 ] );

    return 0;
}

int gaussBlur_gccvectors( float** p_image, float** p_output, int width, int height )
{
    int i = 1;
    for(; i < ( height - 1 ); i++ )
    {
        int j = 1;
        for(; j < ( width - 1 ); j++ )
        {
            p_output[ i ][ j ] = ( 0.5f * p_image[ i ][ j ] ) +
                                 ( 0.125f * p_image[ i - 1 ][ j ] ) +
                                 ( 0.125f * p_image[ i + 1 ][ j ] ) +
                                 ( 0.125f * p_image[ i ][ j - 1 ] ) +
                                 ( 0.125f * p_image[ i ][ j + 1 ] );
        }

        p_output[ i ][ 0 ] = ( 0.625f * p_image[ i ][ 0 ] ) +
                             ( 0.125f * p_image[ i - 1 ][ 0 ] ) +
                             ( 0.125f * p_image[ i + 1 ][ 0 ] ) +
                             ( 0.125f * p_image[ i ][ 1 ] );

        p_output[ i ][ width - 1 ] = ( 0.625f * p_image[ i ][ width - 1 ] ) +
                                     ( 0.125f * p_image[ i - 1 ][ width - 1 ] ) +
                                     ( 0.125f * p_image[ i + 1 ][ width - 1 ] ) +
                                     ( 0.125f * p_image[ i ][ width - 2 ] );
    }

    int j = 1;
    for(; j < ( width - 1 ); j++ )
    {
        p_output[ 0 ][ j ] = ( 0.625f * p_image[ 0 ][ j ] ) +
                             ( 0.125f * p_image[ 1 ][ j ] ) +
                             ( 0.125f * p_image[ 0 ][ j + 1 ] ) +
                             ( 0.125f * p_image[ 0 ][ j - 1 ] );

        p_output[ height - 1 ][ j ] = ( 0.625f * p_image[ width - 1 ][ j ] ) +
                                      ( 0.125f * p_image[ width - 2 ][ j ] ) +
                                      ( 0.125f * p_image[ width - 1 ][ j + 1 ] ) +
                                      ( 0.125f * p_image[ width - 1 ][ j - 1 ] );
    }

    p_output[ 0 ][ 0 ] = ( 0.75f * p_image[ 0 ][ 0 ] ) +
                         ( 0.125f * p_image[ 0 ][ 1 ] ) +
                         ( 0.125f * p_image[ 1 ][ 0 ] );

    p_output[ height - 1 ][ 0 ] = ( 0.75f * p_image[ height - 1 ][ 0 ] ) +
                                  ( 0.125f * p_image[ height - 1 ][ 1 ] ) +
                                  ( 0.125f * p_image[ height - 2 ][ 0 ] );

    p_output[ 0 ][ width - 1 ] = ( 0.75f * p_image[ 0 ][ width - 1 ] ) +
                                 ( 0.125f * p_image[ 0 ][ width - 2 ] ) +
                                 ( 0.125f * p_image[ 1 ][ width - 1 ] );

    p_output[ height - 1 ][ width - 1 ] = ( 0.75f * p_image[ height - 1 ][ width - 1 ] ) +
                                          ( 0.125f * p_image[ height - 1 ][ width - 2 ] ) +
                                          ( 0.125f * p_image[ height - 2 ][ width - 1 ] );

    return 0;
}

