#ifndef __GAUSSBLUR_H__
#define __GAUSSBLUR_H__

int gaussBlur_intrinsics( float** p_image, float** p_output, int width, int height );
int gaussBlur_gccvectors( float** p_image, float** p_output, int width, int height );

#endif
