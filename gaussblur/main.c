#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "gaussblur.h"

#define BUFF_SIZE 2048

//
//  main.c
//
//  Note: the testing subject will not receive a copy of this file
//

int serial_gaussBlur( float** p_image, float** p_output, int width, int height );
float** loadImage( const char* p_filename, int* p_width, int* p_height );
int saveImage( const char* p_filename, float** p_image, int width, int height );

int main( void )
{
    srand( time( NULL ) );

    int width = 0;
    int height = 0;
    float** p_image = loadImage( "image.dat", &width, &height );

    int i = 0;
    float** p_intrinsics_output = ( float** )malloc( height * sizeof( float* ) );
    float** p_gccvectors_output = ( float** )malloc( height * sizeof( float* ) );
    float** p_serial_output = ( float** )malloc( height * sizeof( float* ) );
    for(; i < height; i++ )
    {
        p_intrinsics_output[ i ] = ( float* )malloc( width * sizeof( float ) );
        p_gccvectors_output[ i ] = ( float* )malloc( width * sizeof( float ) );
        p_serial_output[ i ] = ( float* )malloc( width * sizeof( float ) );
    }

    serial_gaussBlur( p_image, p_serial_output, width, height );
    gaussBlur_intrinsics( p_image, p_intrinsics_output, width, height );
    gaussBlur_gccvectors( p_image, p_gccvectors_output, width, height );

    printf( "Checking your intrinsics solution...\n" );
    int badness = 0;
    int* cmp = ( int* )malloc( height * sizeof( int ) );
    for( i = 0; i < height; i++ )
    {
        cmp[ i ] = memcmp( p_intrinsics_output[ i ], p_serial_output[ i ], width * sizeof( float ) );
        if( cmp[ i ] != 0 ) badness = 1;
    }

    if( badness != 0 )
    {
        printf( "You have a bad solution.\n" );
        printf( "Here are the badnesses of your solution by row:\n" );
        printf( "Ordered pairs of row, badness:\n{ " );
        for( i = 0; i < height; i++ )
        {
            printf( "{ %d, %d }", i, cmp[ i ] );
            if( i < ( height - 1 ) )
            {
                printf( ", " );
            }
        }
        printf( " }\n" );
    }
    else
    {
        printf( "Your solution is correct! Can it get any faster?\n" );
    }

    free( cmp );

    printf( "Checking your GCC vector extensions solution...\n" );
    badness = 0;
    cmp = ( int* )malloc( height * sizeof( int ) );
    for( i = 0; i < height; i++ )
    {
        cmp[ i ] = memcmp( p_gccvectors_output[ i ], p_serial_output[ i ], width * sizeof( float ) );
        if( cmp[ i ] != 0 ) badness = 1;
    }

    if( badness != 0 )
    {
        printf( "You have a bad solution.\n" );
        printf( "Here are the badnesses of your solution by row:\n" );
        printf( "Ordered pairs of row, badness:\n{ " );
        for( i = 0; i < height; i++ )
        {
            printf( "{ %d, %d }", i, cmp[ i ] );
            if( i < ( height - 1 ) )
            {
                printf( ", " );
            }
        }
        printf( " }\n" );
    }
    else
    {
        printf( "Your solution is correct! Can it get any faster?\n" );
    }

    free( cmp );

    printf( "Now writing your solutions to file...\n" );
    saveImage( "intrinsics.dat", p_intrinsics_output, width, height );
    saveImage( "gccvectors.dat", p_gccvectors_output, width, height );

    for( i = 0; i < height; i++ )
    {
        free( p_intrinsics_output[ i ] );
        free( p_gccvectors_output[ i ] );
        free( p_serial_output[ i ] );
    }

    free( p_intrinsics_output );
    free( p_gccvectors_output );
    free( p_serial_output );

    return 0;
}

int serial_gaussBlur( float** p_image, float** p_output, int width, int height )
{
    int i = 1;
    for(; i < ( height - 1 ); i++ )
    {
        int j = 1;
        for(; j < ( width - 1 ); j++ )
        {
            p_output[ i ][ j ] = ( 0.5f * p_image[ i ][ j ] ) +
                                 ( 0.125f * p_image[ i - 1 ][ j ] ) +
                                 ( 0.125f * p_image[ i + 1 ][ j ] ) +
                                 ( 0.125f * p_image[ i ][ j - 1 ] ) +
                                 ( 0.125f * p_image[ i ][ j + 1 ] );
        }

        p_output[ i ][ 0 ] = ( 0.625f * p_image[ i ][ 0 ] ) +
                             ( 0.125f * p_image[ i - 1 ][ 0 ] ) +
                             ( 0.125f * p_image[ i + 1 ][ 0 ] ) +
                             ( 0.125f * p_image[ i ][ 1 ] );

        p_output[ i ][ width - 1 ] = ( 0.625f * p_image[ i ][ width - 1 ] ) +
                                     ( 0.125f * p_image[ i - 1 ][ width - 1 ] ) +
                                     ( 0.125f * p_image[ i + 1 ][ width - 1 ] ) +
                                     ( 0.125f * p_image[ i ][ width - 2 ] );
    }

    int j = 1;
    for(; j < ( width - 1 ); j++ )
    {
        p_output[ 0 ][ j ] = ( 0.625f * p_image[ 0 ][ j ] ) +
                             ( 0.125f * p_image[ 1 ][ j ] ) +
                             ( 0.125f * p_image[ 0 ][ j + 1 ] ) +
                             ( 0.125f * p_image[ 0 ][ j - 1 ] );

        p_output[ height - 1 ][ j ] = ( 0.625f * p_image[ width - 1 ][ j ] ) +
                                      ( 0.125f * p_image[ width - 2 ][ j ] ) +
                                      ( 0.125f * p_image[ width - 1 ][ j + 1 ] ) +
                                      ( 0.125f * p_image[ width - 1 ][ j - 1 ] );
    }

    p_output[ 0 ][ 0 ] = ( 0.75f * p_image[ 0 ][ 0 ] ) +
                         ( 0.125f * p_image[ 0 ][ 1 ] ) +
                         ( 0.125f * p_image[ 1 ][ 0 ] );

    p_output[ height - 1 ][ 0 ] = ( 0.75f * p_image[ height - 1 ][ 0 ] ) +
                                  ( 0.125f * p_image[ height - 1 ][ 1 ] ) +
                                  ( 0.125f * p_image[ height - 2 ][ 0 ] );

    p_output[ 0 ][ width - 1 ] = ( 0.75f * p_image[ 0 ][ width - 1 ] ) +
                                 ( 0.125f * p_image[ 0 ][ width - 2 ] ) +
                                 ( 0.125f * p_image[ 1 ][ width - 1 ] );

    p_output[ height - 1 ][ width - 1 ] = ( 0.75f * p_image[ height - 1 ][ width - 1 ] ) +
                                          ( 0.125f * p_image[ height - 1 ][ width - 2 ] ) +
                                          ( 0.125f * p_image[ height - 2 ][ width - 1 ] );

    return 0;
}

float** loadImage( const char* p_filename, int* p_width, int* p_height )
{
    float** p_image = NULL;
    FILE* p_image_file = fopen( p_filename, "r" );

    char buff[ BUFF_SIZE ];
    memset( buff, 0x00, BUFF_SIZE * sizeof( char ) );

    //
    // probably don't need to memset but better safe than sorry
    // don't really care much about speed here.
    //

    if( p_image_file == NULL ) return NULL;

    fgets( buff, BUFF_SIZE, p_image_file );
    *p_width = atoi( buff );
    *p_height = atoi( strchr( buff, ',' ) + 1 );

    if( ( *p_width == 0 ) || ( *p_height == 0 ) )
    {
        fclose( p_image_file );
        return NULL;
    }

    int i = 0;
    p_image = ( float** )malloc( *p_height * sizeof( float* ) );
    for( i = 0; i < *p_height; i++ )
    {
        p_image[ i ] = ( float* )malloc( *p_width * sizeof( float ) );
        fgets( buff, BUFF_SIZE, p_image_file );
        if( buff[ 0 ] == '\0' )
        {
            fclose( p_image_file );
            for(; i >= 0; i-- )
            {
                free( p_image[ i ] );
            }
            free( p_image );
            return NULL;
        }

        char* temp_buf = buff;
        int j = 0;
        for(; j < *p_width; j++ )
        {
            p_image[ i ][ j ] = ( float )atoi( temp_buf ) / 256.0f;
            temp_buf = strchr( temp_buf, ',' ) + 1;
            if( temp_buf == NULL )
            {
                fclose( p_image_file );
                for(; i >= 0; i-- )
                {
                    free( p_image[ i ] );
                }
                free( p_image );
                return NULL;
            }
        }
    }

    fclose( p_image_file );
    return p_image;
}

int saveImage( const char* p_filename, float** p_image, int width, int height )
{
    if( p_image == NULL ) return -1;
    FILE* p_file = fopen( p_filename, "w" );
    if( p_file == NULL ) return -1;

    fprintf( p_file, "%d,%d\n", width, height );

    int i = 0;
    for(; i < height; i++ )
    {
        int j = 0;
        for(; j < width; j++ )
        {
            fprintf( p_file, "%d,", ( int )( p_image[ i ][ j ] * 256.0f ) );
        }
        fprintf( p_file, "\n" );
    }
    fclose( p_file );

    return 0;
}

