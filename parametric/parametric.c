#include "parametric.h"

//
//  parametric.c
//
//  functions: int parametric( void )
//      arguments:
//          float* values: The input values to evaluate with your function
//          float* output: The list of output values. This array should be
//              in flattened format. In other words, every group of four
//              elements should represent one 4-vector, there is no second 
//              dimension 
//          int length: the length of values, 1/4 the length of output
//      function:
//          Compute the parametric equation:
//
//              f[ x_ ] := { x^3, 2x + 1, x^2 / 3, x }
//
//          at the values specified in values, and save the output to
//          flattened vector format in output.
//
//  implement the functions in this file using appropriate vector
//  instructions as directed in the lab instructions.
//

int parametric_intrinsics( float* values, float* output, int length )
{
    int i = 0;
    float* c1 = output;
    float* c2 = output + length;
    float* c3 = c2 + length;
    float* c4 = c3 + length;
    for(; i < length; i++ )
    {
        float t = values[ i ];
        c1[ i ] = t * t * t;
        c2[ i ] = ( 2.0f * t ) + 1.0f;
        c3[ i ] = ( t * t ) / 3.0f;
        c4[ i ] = t;
    }

    return 0;
}

int parametric_gccvectors( float* values, float* output, int length )
{
    int i = 0;
    float* c1 = output;
    float* c2 = output + length;
    float* c3 = c2 + length;
    float* c4 = c3 + length;
    for(; i < length; i++ )
    {
        float t = values[ i ];
        c1[ i ] = t * t * t;
        c2[ i ] = ( 2.0f * t ) + 1.0f;
        c3[ i ] = ( t * t ) / 3.0f;
        c4[ i ] = t;
    }

    return 0;
}

