#ifndef __PARAMETRIC_H__
#define __PARAMETRIC_H__

int parametric_intrinsics( float* values, float* output, int length );
int parametric_gccvectors( float* values, float* output, int length );

#endif
