PROJECT = parametric

BINARY = $(PROJECT).bin

SOURCES = parametric.c

HEADERS = parametric.h

DEADSOURCES = main.c

OBJECTS = $(SOURCES:.c=.o)
DEADOBJECTS = $(DEADSOURCES:.c=.do)

DOCSRC = instructions.tex
DOCS = $(DOCSRC:.tex=.pdf)

CFLAGS = -Wall -pg
LDFLAGS = -Wall -pg

DATAFILE = data.txt

SHELL = bash

all: head $(BINARY) $(DOCS)
	@echo Testing...
	@echo ======================================== BEGIN APPLICATION OUTPUT |& tee -a $(DATAFILE)
	@echo |& tee -a $(DATAFILE)
	@./$(BINARY) |& tee -a $(DATAFILE)
	@echo |& tee -a $(DATAFILE)
	@echo ========================================== END APPLICATION OUTPUT |& tee -a $(DATAFILE)
	@echo parametric.c: &>> $(DATAFILE)
	@cat parametric.c &>> $(DATAFILE)

head:
	@echo &>> $(DATAFILE)
	@echo ================================================================= &>> $(DATAFILE)
	@echo ===================== NEW MAKE INVOCATION ======================= &>> $(DATAFILE)
	@echo ================================================================= &>> $(DATAFILE)
	@date +%s &>> $(DATAFILE)
	@echo Compile stage: &>> $(DATAFILE)

$(BINARY): $(OBJECTS) $(DEADOBJECTS)
	@echo Linking $(BINARY)... |& tee -a $(DATAFILE)
	@gcc $(LDFLAGS) $(OBJECTS) $(DEADOBJECTS) -o $(BINARY) |& tee -a $(DATAFILE)

%.o: %.c
	@echo Building $@... | tee -a $(DATAFILE)
	@gcc $< -c $(CFLAGS) -o $@ |& tee -a $(DATAFILE)

%.do: %.c
	@echo Building $@...
	@gcc $< -c $(CFLAGS) -o $@

%.pdf: %.tex
	@echo Building documentation $@... | tee -a $(DATAFILE)
	@pdflatex -interaction=batchmode $< |& tee -a $(DATAFILE)
	@rm *.log *.aux

clean:
	@rm -f *.o *.bin
	@rm -rf dist
	@rm -f $(PROJECT).zip
	@rm -f *.log *.aux
	@rm -f gmon.out

#
# We require the dead sources here because we don't want users to clean
# and remove things they can't get back unless they have the original
# stuff
#

fullclean: $(DEADSOURCES) clean
	@rm -f *.do
	@rm -f *.pdf
	@rm -f data.txt

fulldist: $(DOCS) $(DEADSOURCES) $(SOURCES) $(HEADERS)
	@rm -rf dist
	@mkdir dist
	@cp makefile dist/
	@cp $(SOURCES) dist/
	@cp $(HEADERS) dist/
	@cp $(DEADSOURCES) dist/
	@cp *.pdf dist/
	@zip -r $(PROJECT).zip dist/
	@cp $(DOCS) dist/

dist: $(DOCS) $(DEADOBJECTS) $(SOURCES) $(HEADERS)
	@rm -rf dist
	@mkdir dist
	@cp makefile dist/
	@cp $(SOURCES) dist/
	@cp $(HEADERS) dist/
	@cp $(DEADOBJECTS) dist/
	@cp *.pdf dist/
	@zip -r $(PROJECT).zip dist/
	@cp $(DOCS) dist/

