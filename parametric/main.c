#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "parametric.h"

#define STARTING_VALUE 0.0f
#define INTERVAL 0.1f
#define NUM_VALUES 100

//
//  main.c
//
//  Note: the testing subject will not receive a copy of this file
//

int serial_parametric( float* values, float* output, int length );

int main( void )
{
    srand( time( NULL ) );

    float values[ NUM_VALUES ];
    float intrinsics_output[ NUM_VALUES * 4 ];
    float gccvectors_output[ NUM_VALUES * 4 ];
    float serial_output[ NUM_VALUES * 4 ];

    float current_value = STARTING_VALUE;
    int i = 0;
    for(; i < NUM_VALUES; i++ )
    {
        values[ i ] = current_value;
        current_value += INTERVAL;
    }

    serial_parametric( values, serial_output, NUM_VALUES );
    parametric_intrinsics( values, intrinsics_output, NUM_VALUES );
    parametric_gccvectors( values, gccvectors_output, NUM_VALUES );

    int intrinsics_success = memcmp( intrinsics_output, serial_output, NUM_VALUES * 4 * sizeof( float ) );
    int gccvectors_success = memcmp( gccvectors_output, serial_output, NUM_VALUES * 4 * sizeof( float ) );

    if( ( intrinsics_success == 0 ) && ( gccvectors_success == 0 ) )
    {
        printf( "Your program functions correctly\n" );
    }
    else
    {
        if( intrinsics_success != 0 )
        {
            printf( "Your intrinsics solution is not correct\n" );
            printf( "Your error difference: %d\n", intrinsics_success );
        }

        if( gccvectors_success != 0 )
        {
            printf( "Your GCC vector extensions solution is not correct\n" );
            printf( "Your error difference: %d\n", gccvectors_success );
        }
    }

    return 0;
}

//
//  the parametric equation that we are evaluating is:
//
//  f[ x_ ] := { x^3, 2x + 1, x^2 / 3, x }

int serial_parametric( float* values, float* output, int length )
{
    int i = 0;
    float* c1 = output;
    float* c2 = output + length;
    float* c3 = c2 + length;
    float* c4 = c3 + length;
    for(; i < length; i++ )
    {
        float t = values[ i ];
        c1[ i ] = t * t * t;
        c2[ i ] = ( 2.0f * t ) + 1.0f;
        c3[ i ] = ( t * t ) / 3.0f;
        c4[ i ] = t;
    }

    return 0;
}

